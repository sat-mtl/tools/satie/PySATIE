.. PySATIE documentation master file, created by
   sphinx-quickstart on Fri Apr  5 15:00:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PySATIE's documentation
=======================


API documentation
-----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Satie
^^^^^

.. automodule:: pysatie.satie
   :members:
   :undoc-members:

Node
^^^^

.. automodule:: pysatie.node
   :members:
   :undoc-members:

Group
^^^^^

.. automodule:: pysatie.group
   :members:
   :undoc-members:

Plugin
^^^^^^

.. automodule:: pysatie.plugin
   :members:
   :undoc-members:

Property
^^^^^^^^

.. automodule:: pysatie.property
   :members:
   :undoc-members:

Oscserver
^^^^^^^^^

.. automodule:: pysatie.oscserver
   :members:
   :undoc-members:

Utils
^^^^^

.. automodule:: pysatie.utils
   :members:
   :undoc-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
