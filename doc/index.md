![logo](./img/pysatie_mini.png)


[PySATIE](https://gitlab.com/sat-metalab/PySATIE) is a python module facilitating control of [SATIE](https://gitlab.com/sat-metalab/SATIE). It is basically a wrapper around SATIE's OSC API, allowing for easy integration into Python scripts. It creates an OSC server and client and provides methods to manipulate SATIE's state.

# Installation

## Dependencies

- [liblo](http://das.nasophon.de/pyliblo) or ``sudo apt install python3-liblo``

## Installing PySATIE module

In order to install [PySATIE](https://gitlab.com/sat-metalab/PySATIE), clone it and from within its directory execute `python -m pip install .` (or `python -m pip install -e .` if you also want to hack at it). You will need to use at least Python 3.6.

# Notes

## Logging

PySATIE makes use of the `logging` module. You can make use of it buy doing something like:

```python
import logging
logging.basicConfig(filename="/tmp/pysatie.log", level=logging.DEBUG)
```

# Usage

Please refer to the [API documentation]()
