import logging

from pysatie import utils
from pysatie.oschandlers import OSChandlers
from pysatie.oscpyserver import OSCserver
# from pysatie.oscserver import OSCserver
from pysatie.node import Node
from pysatie.plugin import Plugin
from pysatie.sclang_process import SclangProcess
from pysatie.signaling import Signaling

from typing import Any, Dict, List, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from blinker import Signal

logger = logging.getLogger(__name__)


class Satie:
    """Handle the connection and interactions with SATIE server.

    This module provides all means to communicate with SATIE. It instantiates an
    OSC server and wraps all the necessary functions for meaningful interaction.
    """

    def __init__(self) -> None:
        """Set some default values and connect to the SATIE server."""
        logging.debug("satie being created")
        self._osc: Optional[OSCserver] = None
        self._sclang: SclangProcess = None
        self._connected: bool = False
        self._nodes: Dict = {}
        self._processes: Dict = {}
        self._plugins: List['Plugin'] = []
        self._satie_active = False
        self._signals = Signaling()
        self._handlers = OSChandlers(self)


    @property
    def osc(self) -> Optional[OSCserver]:
        """The OSC server"""
        return self._osc

    @property
    def sclang(self) -> Optional[SclangProcess]:
        """The SclangProcess thread"""
        return self._sclang

    @property
    def plugins(self) -> List['Plugin']:
        """List registered SATIE plugins"""
        return self._plugins

    @property
    def signals(self) -> Dict[str, 'Signal']:
        """List all subscribable signals"""
        return self._signals._signals

    @property
    def nodes(self) -> Dict[Any, Any]:
        """List registered SATIE plugin instances"""
        return self._nodes

    def get_node_by_name(self, name: str) -> Optional[Plugin]:
        """
        Get an instance of node

        :param name: str Name of the node's instance (string)
        :return: Plugin or None
        """
        if name in self._nodes:
            return self._nodes[name]
        else:
            return None

    def run_satie(self) -> None:
        """Start sclang as an external process with """
        self._sclang = SclangProcess()
        self._sclang.run()

    def initialize(self) -> None:
        """Starts the OSC server thread and tells SATIE where to respond"""
        logger.debug("initialize...")
        self.run_satie()
        self.connect()
        # self.set_responder_destination(self._osc.host, self.osc.port)

    def quit_satie(self) -> None:
        """Quit SATIE along with sclang process"""
        self._sclang.stop()

    def set_connect(self, state: bool) -> None:
        """
        A curtesy method to set connection state. It simply calls connect() or disconnect() according to input.

        :param state: bool Connection state
        """
        if state:
            self.initialize()
        else:
            self.disconnect()

    def connect(self) -> None:
        logging.debug("connecting...")
        started = False
        try:
            self._osc = OSCserver(self)
            started = True
        except Exception as e:
            logger.warning(f"PySATIE could not start OSC server: {e}")
            started = False

        if not started:
            return
        if self._osc is not None:
            # self._osc.start()
            self._connected = True
            self.set_responder_destination("localhost", 18060)
            self.register_osc_handlers()

    def register_osc_handlers(self):
        """Register handlers."""
        if self._connected:
            self._osc.bind("/plugins", self._handlers.handle_plugins)
            self._osc.bind("/arguments", self._handlers.handle_plugin_properties)
            self._osc.bind("/satie/configuration", self._handlers.get_satie_configuration_json)
            self._osc.bind("/satie/status", self._handlers.get_satie_status_string)
            self._osc.bind("/satie/source/parameters", self._handlers.get_synth_parameters_json)

    ########################################
    # SC server interactions

    def dac_test_channel(self, num: int, synthDef: str = "whitenoise", *args: Any) -> None:
        if self._osc:
            self._osc.new_synth(synthDef, 5000 + num, 0, 1, "out", num, *args)

    def stop_test_channel(self, num: int) -> None:
        if self._osc:
            self._osc.set_synth_params(5000 + num, "gate", 0)

    ########################################
    # SC server interactions

    def load_sample(self, name: str, filepath: str) -> None:
        """
        Load a sample from file into a buffer and register it with SATIE.
        Currently limited to file formats handled by SuperCollider (.wav, .aif(f))

        :param name: unique name.
        :param filepath: absolute path to the soundfile.
        """

        if self._osc:
            self._osc.load_sample(name, filepath)

    def disconnect(self) -> None:
        """Close OSC communication with SATIE and exit the server thread"""
        if self._osc:
            self.clear_scene()
            self._osc.stop_server()

    def get_server_option(self, key: str) -> None:
        if self._osc:
            self._osc.get_satie_server_option(key)

    def get_satie_configuration(self) -> None:
        """
        Request SATIE configuration. It will be sent as JSON string via OSC.
        """
        if self._osc:
            self._osc.get_satie_configuration()

    def get_satie_status(self) -> None:
        """
        Request SATIE status. It will be sent as string via OSC.
        """
        if self._osc:
            self._osc.get_satie_status()

    def query_audiosources(self) -> None:
        """
        Query SATIE for a list of resources. It sends an OSC message, there is no guarantee that the message was
        sent or acted upon.
        """
        if self._osc:
            self._osc.load_synth_types()

    def set_responder_destination(self, ip: str, port: int) -> None:
        """
        Set SATIE responder's destination address. Once set, SATIE will respond to all introspection queries
        on that address.

        :param ip: str Hostname or IP
        :param port: int Port number
        """
        if self._osc:
            self._osc.set_responder_destination(ip, port)

    def boot(self) -> None:
        """Call boot on SATIE"""

        if self._osc:
            self._osc.satie_boot()

    def quit(self) -> None:
        """Call quit on SATIE"""

        if self._osc:
            self._osc.satie_quit()

    def reboot(self) -> None:
        """Call reboot, or simply boot, on SATIE"""

        if self._osc:
            self._osc.satie_reboot()

    def configure(self, configuration: str) -> None:
        """Configure SATIE by sending it a stringified JSON configuration"""

        if self._osc:
            self._osc.satie_configure(configuration)

    def clear_scene(self) -> None:
        """Clear SATIE's scene and return its state to default"""
        if self._osc:
            self._osc.scene_clear()

    def plugin_properties(self, plugin: str) -> None:
        """
        :param plugin: str - the name of the plugin
        """
        if self._satie_active and self._osc:
            self._osc.load_properties(plugin)

    def set_renderer_orientation(self, aziDeg: float, eleDeg: float) -> None:
        """
        Set renderer's orientation offset

        :param aziDeg: float - azimuth in degrees
        :param eleDeg: float - elevation in degrees
        """
        if self._osc:
            self._osc.set_orientation(aziDeg, eleDeg)

    def set_renderer_volume(self, volume: float) -> None:
        """
        Set renderer's volume

        :param volume: float - from -10dB to +6dB
        """
        if self._osc:
            self._osc.set_output_db(volume)

    def set_renderer_dim(self, state: bool) -> None:
        """
        Set renderer's dim state

        :param state: bool
        """
        if self._osc:
            self._osc.set_dim(state)

    def set_renderer_mute(self, state: bool) -> None:
        """
        Set renderer's mute state

        :param state: bool
        """
        if self._osc:
            self._osc.set_mute(state)

    def add_plugin(self, plugin: 'Plugin') -> None:
        """
        Add a SATIE plugin to a list of available plugins
        """
        plugin.initialize()
        self._plugins.append(plugin)

    def remove_plugin(self, plugin: 'Plugin') -> None:
        """
        Remove a SATIE plugin from the list
        """
        plug_to_go = self.get_plugin_by_name(plugin.name)
        if plug_to_go:
            self._plugins.remove(plug_to_go)

    def delete_node(self, node_name: str) -> None:
        """
        kill SATIE source node
        """
        if self._osc:
            self._osc.delete_node(node_name)
        if node_name in self._nodes:
            self._nodes.pop(node_name)

    def get_plugins_by_type(self, satie_type: str) -> List['Plugin']:
        return [plugin for plugin in self._plugins
                if plugin._type == satie_type]

    def get_plugin_by_name(self, name: str, category: Optional[str] = None) -> Optional['Plugin']:
        """
        Find plugin by name

        :param name:
        :param category:
        :return:
        """
        return next((plugin for plugin in self._plugins if
                     (category is None or plugin.category == category)
                     and plugin.name == name),
                    None)

    def create_source_node(self, id: str, plugin: 'Plugin', group: Optional[str] = None, **kwargs) -> None:
        """
        Instantiate a generator

        :param object: plugin - a SATIE plugin
        """
        node = Node(self, id, plugin, **kwargs)
        self._nodes[id] = node
        try:
            node.initialize()
            logger.debug(f"node {node} initialized successfully")
            logger.debug(f"it was set up with {id} {plugin} {kwargs}")
            self._satie_active = True
        except AttributeError:
            logger.warning("Could not instantiate a node, perhaps pysatie.satie.Node was not created properly")

    def set_debug(self, debug: bool) -> None:
        """Turn SATIE's debug option on/off

        :param debug: bool on/off
        """
        if self._osc:
            self._osc.set_debug(debug)

    def enable_heartbeat(self, flag: bool) -> None:
        """Turn SATIE's status heartbeat on/off

        :param flag: bool on/off
        """
        if self._osc:
            self._osc.enable_heartbeat(flag)

    def scene_set(self, key: str, value: Any) -> None:
        """
        Set a scene-wide property

        :param key: property
        :param value; value
        """
        if self._osc:
            self._osc.scene_set(key, value)

    ########################################
    # Node messages

    def node_set(self, node_name: str, *args: Any, node_type: str='source') -> None:
        """
        Set some node properties. Capable of setting many properties via one command

        :param node_name: the id of the node
        :param args: property/value pairs.
        :param node_type: (string) - the type of node, either source, group or process
        """
        if self._osc:
            synth_args = utils.list_to_dict(args)
            for key in synth_args.keys():
                self._nodes[node_name].plugin.synth_args[key] = synth_args[key]
            self._osc.node_set(node_type, node_name, *args)

    def node_state(self, node_name: str, state: int, node_type: str = 'source') -> None:
        """
        Set DSP state of a nodes

        :param node_name: - the id of the node
        :param state: 1 = on, 0 = off.
        :param node_type: - the type of node, either source, group or process
        """
        if self._osc:
            self._osc.node_state(node_type, node_name, state)

    def node_event(self, node_type: str, node_name: str, event_name: str, *args: Any) -> None:
        """
        Execute an event associated with a node.

        :param node_type: either source, group or process
        :param node_name: instance name
        :param event_name: event name (usually a method defined by source, effect or process)
        :param \*args: optional parameters required by the event (as a sequence)
        """
        if self._osc:
            self._osc.node_event(node_type, node_name, event_name, *args)

    def node_set_vector(self, node_name: str, param: str, *args: Any, node_type: str = 'source') -> None:
        """
        Set vector

        :param node_name: the id of the node
        :param node_type: the type of node, either source, group or process
        :param key: parameter name
        :param args: sequence of values
        """
        if self._osc:
            self._osc.node_set_vector(node_type, node_name, param, *args)

    def node_update(self,
                    node_name: str,
                    azimuth: float,
                    elevation: float,
                    gain: float,
                    delay: float,
                    lph: float,
                    node_type: str = 'source') -> None:
        """
        Update node properties

        :param node_type: source/effect/process
        :param node_name: unique instance name
        :param azimuth: horizontal angle - degrees
        :param elevation: vertical angle - degrees
        :param gain: decibels
        :param delay: milliseconds
        :param lph: low-pass filter frequency in Herz
        """
        if self._osc:
            self._nodes[node_name].azimuth = azimuth
            self._nodes[node_name].elevation = elevation
            self._nodes[node_name].gain = gain
            self._nodes[node_name].delay = delay
            self._nodes[node_name].low_pass = lph
            self._osc.node_update(node_type, node_name, azimuth, elevation, gain, delay, lph)

    def get_synth_parameters(self, synth: str, group='default') -> None:
        """
        Request SATIE's running synth parameters. It will be sent as JSON string via OSC.
        """
        if self._osc:
            self._osc.get_synth_parameters(synth, group)

    def get_synth_value(self, key: str, synth: str, group='default') -> None:
        """
        Request the value of a given parameter of a running synth in SATIE. It will be sent as JSON string via OSC.
        """
        if self._osc:
            self._osc.get_synth_value(key, synth, group)

    ########################################
    # Handling groups

    def create_source_group(self, group_name: str) -> None:
        """
        Create an audio sources group on the server

        :param group_name: a name of the group
        """
        if self._osc:
            self._osc.create_source_group(group_name)

    def create_effect_group(self, group_name: str) -> None:
        """
        Create an audio effects group on the server

        :param group_name: a name of the group
        """
        if self._osc:
            self._osc.create_effect_group(group_name)

    def create_process_group(self, group_name: str) -> None:
        """
        Create an audio effects group on the server

        :param group_name: a name of the group
        """
        if self._osc:
            self._osc.create_process_group(group_name)


    ####################################################
    # Handling processes

    def create_process_node(self, id: str, process_name: str, **kwargs) -> None:
        """
        Instantiate a SATIE process and add a reference to the dictionary

        :param id: A unique identifier for the process
        :param process_name: The name of the process as registered with SATIE
        :
        """
        if self._osc:
            self._osc.create_process(id, process_name, **kwargs)

    def remove_process_node(self, process_id: str) -> None:
        """
        Kill a SATIE process and remove the reference from the dictionary
        (It's an alias to the generic remove_source_node method)
        """
        if self._osc:
            self._osc.delete_node(process_id)

    def set_process_property(self, node_name: str, *args: Any) -> None:
        """
        Set process properties

        :param node_name: process instance name
        :param \*args: a sequence of key/value pairs
        """
        if self._osc:
            self._osc.process_property(node_name, *args)

    def eval_process_method(self, node_name: str, handler: str, *args: Any) -> None:
        """
        Evaluate a process method

        :param node_name: process instance name
        :param handler: name of the method to be evaluated
        :param args: any arguments required for the method

        """
        if self._osc:
            self._osc.process_eval(node_name, handler, *args)

    def set_postprocessor_property_value(self, key: str, value: Any, postproc_type: str='postproc') -> None:
        """
        Set a single property on a post-processor.

        :param key: name of the property
        :param val: property's value
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``

        A post-processor is a singleton containing a pipeline of synths.
        """
        if self._osc:
            self._osc.set_postprocessor_property_value(key, value, postproc_type)

    def set_postprocessor_property_vector(self, key: str, postproc_type: str='postproc', *args: Any) -> None:
        """
        Set a post-processor property with a vector

        :param key: name of the property
        :param *args: a sequence of values
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``

        """
        if self._osc:
            self._osc.set_postprocessor_property_vector(key, postproc_type, *args)


    def apply_postprocessor(self, postproc_type: str="postproc") -> None:
        """
        Apply the configured post-processor

        """
        if self._osc:
            self._osc.apply_postprocessor(postproc_type)

    def set_postprocessor_pipeline_property(self, name: str, *args: Any) -> None:
        """
        Set a property of the pipeline

        :param name: name of the postprocessor (project dependent)
        :param *args: key, value pairs for properties to affect
        """
        if self._osc:
            self._osc.set_postprocessor_pipeline_property(name, *args)

    # Signaling
    def emit(self, name) -> None:
        """
        Emit a signal by name

        :param name: name of a signal
        """
        self._signals._signals[name].send(name)

    def emit_data(self, name, **kw) -> None:
        """
        Emit a signal by name with data

        :param name: name of a signal
        :param **kw: a dictionary of data
        """
        self._signals._signals[name].send(name, **kw)
