"""
This is a SATIE group.
"""

class Group:

    def __init__(self, satie, name):

        self._satie = satie
        self._name = name

    @property
    def name(self):
        return self._name

    def initialize(self):
        """
        Initialize this group instance

        :return:
        """

        # Send OSC message to create the group
        self._satie.osc.scene_create_group(self._name)
