import logging

from typing import Any, Dict, List, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.satie import Satie
    from pysatie.property import Property


logger = logging.getLogger(__name__)

class Plugin:
    def __init__(self, satie: 'Satie', name: str, satie_type: str, category: str, description: str) -> None:
        self._satie: Satie = satie
        self._name: str = name       # name assigned by makeSynthDef
        self._type: str = satie_type
        self._category: str = category
        self._description: str = description
        self._properties: List[Property] = []
        self._synth_args: Optional[Dict[str, Any]] = {}

    @property
    def synth_args(self) -> Optional[Dict[str, Any]]:
        return self._synth_args

    @synth_args.setter
    def synth_args(self, initial_properties: Dict[str, Any]) -> None:
        self._synth_args = initial_properties

    @property
    def satie(self) -> 'Satie':
        return self._satie

    @property
    def name(self) -> str:
        return self._name

    @property
    def type(self) -> str:
        return self._type

    @property
    def category(self) -> str:
        return self._category

    @property
    def description(self) -> str:
        return self._description

    @description.setter
    def description(self, value: str) -> None:
        self._description = value

    @property
    def properties(self) -> List['Property']:
        return self._properties

    @properties.setter
    def properties(self, props: List['Property']) -> None:
        """
        Set properties
        :props: list
        """
        self._properties = props

    def initialize(self) -> None:
        if self._satie.osc:
            self._satie.osc.load_properties(self._name)

    def create_source_instance(self, id: str, group: str) -> None:
        """
        Create an instance of the plugin

        :param id: unique id
        :param group: SATIE group
        :return:
        """
        if self._satie.osc:
            if self._synth_args:
                self._satie.osc.create_source(id, self._name, group, **self._synth_args)
                logger.debug("creating a source: with {id} {self._name}, {group}, {**self._synth_args}")
            else:
                self._satie.osc.create_source(id, self._name, group)

    def create_fx_instance(self, id: str, group: str, bus: int = 0) -> None:
        """
        Create an instance of the plugin

        :param id: a unique id
        :param group: SATIE group
        :param bus: aux bus number
        :return:
        """
        if self._satie.osc:
            self._satie.osc.create_effect(id, self._name, group, bus)
            logger.debug("creating effect: with {id} {self._name}, {group}, {bus}")
